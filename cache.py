from robot import Robot


class Cache:
    def __init__(self):
        self.downloaded = {}

    def retrieve(self, url):
        robot = Robot(url)
        data = robot.retrieve()
        return data

    def content(self, url):
        if url not in self.downloaded.keys():
            data = self.retrieve(url)
            self.downloaded[url] = data

    def show(self, url):
        self.content(url)
        print(self.downloaded[url])

    def show_all(self):
        print("Lista de paginas web cuyos documentos han sido descargados: ")
        for url in self.downloaded.keys():
            print(url)
