from urllib import request


class Robot:
    def __init__(self, url):
        self.url = url
        self.downloaded = False

    def retrieve(self):
        if not self.downloaded:
            print("Descargando {}".format(self.url))
            response = request.urlopen(self.url)
            data = response.read().decode("UTF-8")
            self.downloaded = True
            return data

    def content(self):
        if not self.downloaded:
            data = self.retrieve()
            self.content = data
        return self.content

    def show(self):
        print(self.content())
