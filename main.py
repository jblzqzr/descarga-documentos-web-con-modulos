from robot import Robot
from cache import Cache

if __name__ == '__main__':

    print("*********** ROBOT 1 ***********")
    robot = Robot("http://gsyc.urjc.es/")
    print("Robot 1: {}".format(robot.url))
    robot.show()

    print("*********** ROBOT 2 ***********")
    robot2 = Robot("https://www.urjc.es/")
    print("Robot 2: {}".format(robot2.url))
    robot2.show()

    print("*********** CACHE 1 ***********")
    cache = Cache()
    cache.content('http://gsyc.urjc.es/')
    cache.content("https://www.aulavirtual.urjc.es/moodle/")
    cache.show_all()

    print("*********** CACHE 2 ***********")
    cache2 = Cache()
    cache2.content("https://docs.python.org/")
    cache2.content("https://docs.python.org/")
    cache2.show_all()